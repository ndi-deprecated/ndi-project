Deprecated
==========

Le but de notre application était de faciliter la reprise de contact entre un réfugié et ses connaissances. 
Les réfugiés sont enregistré auprès de leur camps par des responsables de l’ONG en charge du camp. 
Les proches en recherche peuvent créer un compte sur le site afin de voir si un profil correspond à leur description. 
Si la requête échoue, il est possible de la sauvegarder afin de détecter de nouveaux réfugiés correspondant aux critères afin permettre une veille.


A propos sur l’architecture générale de notre appli, il s’agit d’une traditionelle architecture 3 tiers. 
Le serveur a été dévelopé en java en se basant sur le framework spring-boot, et bien évidemment le framework AngularJs
a été utilisé coté client, ainsi que bootstrap.
La documentation de notre API REST est disponible [en ligne sur Apiary](http://docs.deprecatedndi.apiary.io/)


La liste non exhaustive de notre environnement de travail et les outils utilisés comprend:
Eclipse et Atom pour le développement
SourceTree Git bash pour interagir en local avec le dépot git
Bitbucket en tant que repositery git pour le partage du code
Heroku en temps que cloud Paas pour faire tourner notre Api
Trello, pour une gestion des taches agiles version Kanban
Slack pour la communication asynchrone intraéquipe
Un tableau, des feutres et quelques neurones.
L’application est accessible sur le cloud, et le code est téléchargeable sous forme d’archive zip ici.
Merci pour cette incroyable soirée très amusante :) 



Application explorable en ligne [dans le cloud](https://ndi2014.herokuapp.com) réalisée lors
de la [nuit de l'info](http://www.nuitdelinfo.com/n2i) 

*Deprecated - as of December 5th 2014*