package fr.ndi.deprecated.service;

import java.util.List;

import fr.ndi.deprecated.domain.OngMember;

public interface OngMemberService {

	OngMember get(Long id);

	List<OngMember> getAll();

	OngMember save(OngMember user);

	OngMember findByMail(String mail);

	boolean validateCredentials(String mail, String password);
}
