package fr.ndi.deprecated.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import fr.ndi.deprecated.domain.OngMember;
import fr.ndi.deprecated.repository.OngMemberRepository;
import fr.ndi.deprecated.service.HashGeneratorService;
import fr.ndi.deprecated.service.OngMemberService;

@Validated
@Service
public class OngMemberServiceImpl implements OngMemberService {

	@Autowired
	private OngMemberRepository repository;

	@Autowired
	private HashGeneratorService hashGeneratorService;

	@Override
	public OngMember get(Long id) {
		return repository.findOne(id);
	}

	@Override
	public List<OngMember> getAll() {
		return (List<OngMember>) repository.findAll();
	}

	@Override
	public OngMember save(OngMember user) {	
		return repository.save(user);
	}

	@Override
	public OngMember findByMail(String mail) {
		return  repository.findByMail(mail);
	}

	public boolean validateCredentials(String mail, String password) {
		OngMember match = repository.findByMail(mail);
		// no user with this username
		if (match == null) {
			return false;
		}

		// compute salted hash of password
		String hashedPassword = hashGeneratorService.getSaltedHash(password,
				match.getSalt());
		return match.getPassword().equals(hashedPassword);
	}

}
