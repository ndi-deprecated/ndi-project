package fr.ndi.deprecated.service.impl;

import org.springframework.stereotype.Service;

import fr.ndi.deprecated.domain.HashedPasswordTuple;
import fr.ndi.deprecated.security.SaltedHashGenerator;
import fr.ndi.deprecated.service.HashGeneratorService;

@Service
public class HashGeneratorServiceImpl implements HashGeneratorService {

	@Override
	public HashedPasswordTuple generateHash(String password) {
		return SaltedHashGenerator.hashPasswordUser(password);
	}

	@Override
	public String getSaltedHash(String password, int salt) {
		return SaltedHashGenerator.getSaltedHash(password, salt);
	}

}
