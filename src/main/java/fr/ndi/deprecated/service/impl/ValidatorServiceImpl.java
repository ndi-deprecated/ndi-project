package fr.ndi.deprecated.service.impl;

import org.apache.commons.validator.routines.RegexValidator;
import org.springframework.stereotype.Service;

import fr.ndi.deprecated.service.ValidatorService;

@Service
public class ValidatorServiceImpl implements ValidatorService {

	private RegexValidator usernameValidator = new RegexValidator(
			"[a-z][0-9a-z]{1,15}", false);

	private RegexValidator passwordValidator = new RegexValidator(".{6,32}",
			false);

	@Override
	public boolean validateUsername(String username) {
		return usernameValidator.isValid(username);
	}

	@Override
	public boolean validatePassword(String password) {
		return passwordValidator.isValid(password);
	}
}
