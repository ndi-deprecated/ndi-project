package fr.ndi.deprecated.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ndi.deprecated.domain.RequestRefugee;
import fr.ndi.deprecated.repository.RequestRefugeeRepository;
import fr.ndi.deprecated.service.RequestRefugeeService;

@Service
public class RequestRefugeeServiceImpl implements RequestRefugeeService {
	@Autowired
	RequestRefugeeRepository repository;

	public List<Boolean> findByFinded(boolean finded) {
		return repository.findByFinded(finded);
	}

	public RequestRefugee save(RequestRefugee requestRefugee) {
		return repository.save(requestRefugee);
	}
}
