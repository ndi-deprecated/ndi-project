package fr.ndi.deprecated.service.impl;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.ndi.deprecated.domain.OngMember;
import fr.ndi.deprecated.domain.Seeker;
import fr.ndi.deprecated.domain.Session;
import fr.ndi.deprecated.repository.SessionRepository;
import fr.ndi.deprecated.service.OngMemberService;
import fr.ndi.deprecated.service.SeekerService;
import fr.ndi.deprecated.service.SessionService;

@Service
public class SessionServiceImpl implements SessionService {
	private static final int SESSION_EXPIRY_HOUR = 1;

	@Autowired
	private SessionRepository repository;

	@Autowired
	private OngMemberService ongMemberService;

	@Autowired
	private SeekerService seekerService;

	@Transactional(readOnly = true)
	@Override
	public Session get(Long id) {
		return repository.findOne(id);
	}

	@Transactional
	@Override
	public Session save(Session session) {
		return repository.save(session);
	}

	@Transactional(readOnly = true)
	@Override
	public Session findByToken(String token) {
		return repository.findByToken(token);
	}

	@Override
	public void delete(Session session) {
		repository.delete(session);
	}

	@Override
	public boolean isValidSession(Session session) {
		Calendar calSession = Calendar.getInstance();
		Date dateNow = new Date();

		calSession.setTime(session.getDate());
		calSession.add(Calendar.HOUR, SESSION_EXPIRY_HOUR);

		if (calSession.getTime().before(dateNow)) {
			this.delete(session);
			return false;
		}

		return true;
	}

	@Override
	public Session create(String mail, String ip) {
		OngMember ongMember = ongMemberService.findByMail(mail);
		Seeker seeker = null;
		if (ongMember == null) {
			seeker = seekerService.findByMail(mail);
			if (seeker == null) {
				return null;
			} else {
				return save(new Session(seeker, ip));
			}
		} else {
			return save(new Session(ongMember, ip));
		}
	}
}
