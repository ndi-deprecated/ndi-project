package fr.ndi.deprecated.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ndi.deprecated.domain.OngMember;
import fr.ndi.deprecated.domain.Seeker;
import fr.ndi.deprecated.repository.SeekerRepository;
import fr.ndi.deprecated.service.HashGeneratorService;
import fr.ndi.deprecated.service.SeekerService;

@Service
public class SeekerServiceImpl implements SeekerService {
	@Autowired
	private SeekerRepository repository;
	@Autowired
	private HashGeneratorService hashGeneratorService;

	public Seeker findByMail(String mail) {
		return repository.findByMail(mail);
	}

	@Override
	public Seeker save(Seeker seeker) {
		return repository.save(seeker);
	}

	@Override
	public boolean validateCredentials(String mail, String password) {
		Seeker match = repository.findByMail(mail);
		// no user with this username
		if (match == null) {
			return false;
		}

		// compute salted hash of password
		String hashedPassword = hashGeneratorService.getSaltedHash(password,
				match.getSalt());
		return match.getPassword().equals(hashedPassword);
	}

}
