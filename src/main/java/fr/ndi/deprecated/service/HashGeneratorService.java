package fr.ndi.deprecated.service;

import fr.ndi.deprecated.domain.HashedPasswordTuple;

public interface HashGeneratorService {

	HashedPasswordTuple generateHash(String password);

	String getSaltedHash(String password, int salt);
}
