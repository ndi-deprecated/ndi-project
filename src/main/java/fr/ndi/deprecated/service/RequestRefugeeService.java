package fr.ndi.deprecated.service;

import java.util.List;

import fr.ndi.deprecated.domain.RequestRefugee;

public interface RequestRefugeeService {

	List<Boolean> findByFinded(boolean finded);

	RequestRefugee save(RequestRefugee requestRefugee);
}
