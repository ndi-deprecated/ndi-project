package fr.ndi.deprecated.service;

import fr.ndi.deprecated.domain.OngMember;
import fr.ndi.deprecated.domain.Seeker;

public interface SeekerService {
	Seeker findByMail(String mail);

	Seeker save(Seeker seeker);

	boolean validateCredentials(String mail, String password);

}
