package fr.ndi.deprecated.service;

public interface ValidatorService {

	boolean validateUsername(String username);

	boolean validatePassword(String password);
}
