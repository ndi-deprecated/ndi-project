package fr.ndi.deprecated.service;

import fr.ndi.deprecated.domain.Session;

public interface SessionService {

	Session get(Long id);

	Session save(Session session);

	Session findByToken(String token);

	void delete(Session session);

	boolean isValidSession(Session session);

	Session create(String username, String ip);
}
