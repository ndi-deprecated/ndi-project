package fr.ndi.deprecated.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.ndi.deprecated.domain.Refugee;
import fr.ndi.deprecated.repository.RefugeeRepository;

@RestController
@EnableAutoConfiguration
public class RefugeeController {
	@Autowired
	RefugeeRepository repository;

	@RequestMapping(value = "/refugee", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Object> register(@RequestBody Refugee refugee) throws Exception {
		repository.save(refugee);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/refugee", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Object> getByName(@RequestBody Refugee refugee) throws Exception {
		List<Refugee> lRefugee = repository.findByFirstNameAndLastName(refugee.getFirstName(), refugee.getLastName());
		return new ResponseEntity<Object>(lRefugee.isEmpty() ? null : lRefugee.get(0), HttpStatus.OK);
	}
}
