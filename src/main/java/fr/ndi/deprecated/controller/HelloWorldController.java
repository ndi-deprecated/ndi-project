package fr.ndi.deprecated.controller;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
public class HelloWorldController {
	@RequestMapping(value = "hello", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Object> register() throws Exception {
		return new ResponseEntity<Object>("Hello world", HttpStatus.OK);
	}
}
