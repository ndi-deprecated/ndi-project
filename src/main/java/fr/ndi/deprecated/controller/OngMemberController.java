package fr.ndi.deprecated.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.ndi.deprecated.domain.Ong;
import fr.ndi.deprecated.domain.OngMember;
import fr.ndi.deprecated.domain.Session;
import fr.ndi.deprecated.repository.OngRepository;
import fr.ndi.deprecated.service.OngMemberService;
import fr.ndi.deprecated.service.SessionService;
import fr.ndi.deprecated.service.ValidatorService;

@RestController
@RequestMapping("/OngMember")
public class OngMemberController {

	@Autowired
	private OngMemberService ongMemberService;
	@Autowired
	private SessionService sessionService;
	@Autowired
	private ValidatorService validatorService;

	@Autowired
	private OngRepository ongRepo;

	// fuck the service

	/**
	 * Signup REST
	 * 
	 * @param user
	 * @return Return the person entity if successful
	 */
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<OngMember> signup(
			@RequestParam(value = "fisrtName", required = true) String firstName,
			@RequestParam(value = "lastName", required = true) String lastName,
			@RequestParam(value = "phone", required = true) String phone,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "mail", required = true) String mail,
			@RequestParam(value = "ongId", required = true) Long ongId) {

		if (!EmailValidator.getInstance().isValid(mail)
				|| !validatorService.validatePassword(password)) {
			return new ResponseEntity<OngMember>(HttpStatus.NOT_ACCEPTABLE);
		}

		Ong ong = ongRepo.findOne(ongId);
		if (ong != null) {
			// TODO: log
			// Not an ong!
			return new ResponseEntity<OngMember>(HttpStatus.NOT_ACCEPTABLE);
		}

		OngMember ongMember = new OngMember(ong, firstName, lastName, phone,
				mail, password);
		ongMember = ongMemberService.save(ongMember);

		if (ongMember == null) {
			//FIXME Why BAD requesty?
			return new ResponseEntity<OngMember>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<OngMember>(ongMember, HttpStatus.CREATED);
	}

	/**
	 * Signin REST
	 * 
	 * @return Return the session entity if successful
	 */
	@RequestMapping(value = "/signin", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Session> signin(
			@RequestParam(value = "mail", required = true) String mail,
			@RequestParam(value = "password", required = true) String password,
			HttpServletRequest request) {

		// validate credentials
		if (!ongMemberService.validateCredentials(mail, password)) {
			return new ResponseEntity<Session>(HttpStatus.FORBIDDEN);
		}

		// create session
		Session session = sessionService.create(mail, request.getRemoteAddr());

		if (session == null) {
			return new ResponseEntity<Session>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Session>(session, HttpStatus.OK);
	}

	/**
	 * Signout REST
	 * 
	 * @return Return the session entity if successful
	 */
	@RequestMapping(value = "/signout", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Void> signout(
			@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "mail", required = true) String mail) {

		// retrieve session
		Session session = sessionService.findByToken(token);

		if (session == null || !session.getOngMember().getMail().equals(mail)) {
			return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
		}
		sessionService.delete(session);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}
