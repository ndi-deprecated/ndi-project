package fr.ndi.deprecated.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.ndi.deprecated.domain.Ong;
import fr.ndi.deprecated.domain.OngMember;
import fr.ndi.deprecated.repository.OngMemberRepository;
import fr.ndi.deprecated.repository.OngRepository;

@RestController
@EnableAutoConfiguration
public class OngController {
	@Autowired
	OngRepository ongRep;

	@Autowired
	OngMemberRepository ongMemberRep;

	@RequestMapping(value = "/ong", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Object> register(@RequestBody Ong ong) throws Exception {
		ongRep.save(ong);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/ong", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Object> getONGByName(@RequestBody Ong ong) throws Exception {
		List<Ong> lONG;
		if (ong != null) {
			lONG = ongRep.findByName(ong.getName());
		} else {
			lONG = new ArrayList<Ong>();
			for (Ong o : ongRep.findAll())
				lONG.add(o);
			ongRep.findAll();
		}
		return new ResponseEntity<Object>(ongRep, HttpStatus.OK);
	}

	@RequestMapping(value = "/ong/member", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Object> getMemberOfONG(@RequestBody Ong ong) throws Exception {
		List<Ong> lONG = ongRep.findByName(ong.getName());
		List<Long> memberIds = lONG.isEmpty() ? null : lONG.get(0).getMemberIds();
		List<OngMember> members = new ArrayList<OngMember>();
		for (Long id : memberIds) {
			members.add(ongMemberRep.findOne(id));
		}
		return new ResponseEntity<Object>(members, HttpStatus.OK);
	}
}
