package fr.ndi.deprecated.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.ndi.deprecated.domain.RequestRefugee;
import fr.ndi.deprecated.service.RequestRefugeeService;

@RestController
public class RequestRefugeeController {

	@Autowired
	RequestRefugeeService requestRefugeeService;

	@RequestMapping(value = "/requestRefugee", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Object> create(
			@RequestBody RequestRefugee requestRefugee) throws Exception {
		requestRefugeeService.save(requestRefugee);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

}
