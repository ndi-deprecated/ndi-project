package fr.ndi.deprecated.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.validator.routines.EmailValidator;
import org.hibernate.dialect.FirebirdDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.ndi.deprecated.domain.OngMember;
import fr.ndi.deprecated.domain.Session;
import fr.ndi.deprecated.service.OngMemberService;
import fr.ndi.deprecated.service.SeekerService;
import fr.ndi.deprecated.service.SessionService;
import fr.ndi.deprecated.service.ValidatorService;
import fr.ndi.deprecated.domain.Seeker;

@RestController
@RequestMapping("/Seeker")
public class SeekerController {

	@Autowired
	private SeekerService seekerService;
	@Autowired
	private SessionService sessionService;
	@Autowired
	private ValidatorService validatorService;

	/**
	 * Signup REST
	 * 
	 * @param user
	 * @return Return the person entity if successful
	 */
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Seeker> signup(
			@RequestParam(value = "firstName", required = true) String firstName,
			@RequestParam(value = "lastName", required = true) String lastName,
			@RequestParam(value = "phone", required = false) String phone,
			@RequestParam(value = "country", required = true) String country,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "mail", required = true) String mail) {

		if (!EmailValidator.getInstance().isValid(mail)) {
			return new ResponseEntity<Seeker>(HttpStatus.NOT_ACCEPTABLE);
		}
		if (!validatorService.validatePassword(password)) {
			return new ResponseEntity<Seeker>(HttpStatus.NOT_ACCEPTABLE);
		}
		Seeker seeker = new Seeker(lastName, firstName, country, phone, mail, password);
		seeker = seekerService.save(seeker);

		if (seeker == null) {
			return new ResponseEntity<Seeker>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Seeker>(seeker, HttpStatus.CREATED);
	}

	/**
	 * Signin REST
	 * 
	 * @return Return the session entity if successful
	 */
	@RequestMapping(value = "/signin", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Session> signin(
			@RequestParam(value = "mail", required = true) String mail,
			@RequestParam(value = "password", required = true) String password,
			HttpServletRequest request) {

		// validate credentials
		if (!seekerService.validateCredentials(mail, password)) {
			return new ResponseEntity<Session>(HttpStatus.FORBIDDEN);
		}

		// create session
		Session session = sessionService.create(mail, request.getRemoteAddr());

		if (session == null) {
			return new ResponseEntity<Session>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Session>(session, HttpStatus.OK);
	}

	/**
	 * Signout REST
	 * 
	 * @return Return the session entity if successful
	 */
	@RequestMapping(value = "/signout", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Void> signout(
			@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "mail", required = true) String mail) {

		// retrieve session
		Session session = sessionService.findByToken(token);

		if (session == null || !session.getOngMember().getMail().equals(mail)) {
			return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
		}
		sessionService.delete(session);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}
