package fr.ndi.deprecated.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.ndi.deprecated.domain.Camp;
import fr.ndi.deprecated.repository.CampRepository;

@RestController
@EnableAutoConfiguration
public class CampController {
	@Autowired
	CampRepository repository;

	@RequestMapping(value = "/camps", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Object> register(@RequestBody Camp camp) throws Exception {
		repository.save(camp);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/camps", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Object> getCampByOrganization(@RequestBody Camp campArgs) throws Exception {
		List<Camp> camps;
		if (campArgs.getOrganization() != null) {
			camps = repository.findByOrganization(campArgs.getOrganization());
		} else {
			camps = new ArrayList<Camp>();
			for (Camp camp : repository.findAll())
				camps.add(camp);
		}
		return new ResponseEntity<Object>(camps, HttpStatus.OK);
	}

	@RequestMapping(value = "/camp", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Object> getCampByName(@RequestBody Camp camp) throws Exception {
		List<Camp> lCamp = repository.findByName(camp.getName());
		return new ResponseEntity<Object>(lCamp.isEmpty() ? null : lCamp.get(0), HttpStatus.OK);
	}

}
