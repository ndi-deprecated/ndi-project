package fr.ndi.deprecated.repository;

import org.springframework.data.repository.CrudRepository;

import fr.ndi.deprecated.domain.Seeker;

public interface SeekerRepository extends CrudRepository<Seeker, Long> {
	Seeker findByMail(String mail);

}
