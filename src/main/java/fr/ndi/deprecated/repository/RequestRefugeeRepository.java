package fr.ndi.deprecated.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.ndi.deprecated.domain.RequestRefugee;

public interface RequestRefugeeRepository extends
		CrudRepository<RequestRefugee, Serializable> {

	List<Boolean> findByFinded(boolean finded);

	RequestRefugee save(RequestRefugee requestRefugee);
}
