package fr.ndi.deprecated.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.ndi.deprecated.domain.Ong;

public interface OngRepository extends CrudRepository<Ong, Long> {
	List<Ong> findByName(String name);
}
