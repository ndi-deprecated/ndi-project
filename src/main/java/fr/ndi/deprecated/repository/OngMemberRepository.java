package fr.ndi.deprecated.repository;

import org.springframework.data.repository.CrudRepository;

import fr.ndi.deprecated.domain.OngMember;

public interface OngMemberRepository extends CrudRepository<OngMember, Long> {
	OngMember findByMail(String mail);

}
