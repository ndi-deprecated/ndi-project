package fr.ndi.deprecated.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.ndi.deprecated.domain.Refugee;

public interface RefugeeRepository extends CrudRepository<Refugee, Long> {

	List<Refugee> findByFirstNameAndLastName(String firstName, String lastName);

	List<Refugee> findByFirstNameAndLastNameAndCountry(String firstName,
			String lastName, String country);
}
