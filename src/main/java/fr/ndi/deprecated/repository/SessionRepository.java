package fr.ndi.deprecated.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import fr.ndi.deprecated.domain.Session;

public interface SessionRepository extends CrudRepository<Session, Long> {

	@Query("select s from Session s where s.token = ?")
	Session findByToken(String token);
}
