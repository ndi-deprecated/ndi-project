package fr.ndi.deprecated.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.ndi.deprecated.domain.Camp;

public interface CampRepository extends CrudRepository<Camp, Long>{
	List<Camp> findByName(String name);
	List<Camp> findByOrganization(String organization);
}
