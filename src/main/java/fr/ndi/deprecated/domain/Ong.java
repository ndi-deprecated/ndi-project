package fr.ndi.deprecated.domain;

import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

/**
 * A None Governmental Organization
 * 
 * @author @Deprecated
 *
 */
@Entity
public class Ong {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;

	@ElementCollection
	@CollectionTable(name = "ong_has_member", joinColumns = @JoinColumn(name = "id"))
	private List<Long> memberIds;

	@ElementCollection
	@CollectionTable(name = "ong_has_camp", joinColumns = @JoinColumn(name = "id"))
	private List<Long> campsId;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the employeIds
	 */
	public List<Long> getMemberIds() {
		return memberIds;
	}

	/**
	 * @param employeIds
	 *            the employeIds to set
	 */
	public void setEmployeIds(List<Long> employeIds) {
		this.memberIds = employeIds;
	}

	public int getSizeMember() {
		return memberIds.size();
	}
}
