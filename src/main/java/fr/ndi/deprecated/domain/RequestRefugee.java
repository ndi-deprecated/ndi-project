package fr.ndi.deprecated.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * A request about an actually unknown refugee
 * 
 * @author @Deprecated
 *
 */
@Entity
public class RequestRefugee {

	@Id
	@Column(name = "id", nullable = false, updatable = false)
	@GeneratedValue
	private Long id;

	@ManyToOne
	private Seeker seeker;

	@ManyToOne
	private OngMember ongmember;

	@ManyToOne
	private Refugee refugee;

	@NotNull
	@Column(nullable = false)
	private boolean finded;

	@NotNull
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the seeker
	 */
	public Seeker getSeeker() {
		return seeker;
	}

	/**
	 * @param seeker
	 *            the seeker to set
	 */
	public void setSeeker(Seeker seeker) {
		this.seeker = seeker;
	}

	/**
	 * @return the ongmember
	 */
	public OngMember getOngmember() {
		return ongmember;
	}

	/**
	 * @param ongmember
	 *            the ongmember to set
	 */
	public void setOngmember(OngMember ongmember) {
		this.ongmember = ongmember;
	}

	/**
	 * @return the refugee
	 */
	public Refugee getRefugee() {
		return refugee;
	}

	/**
	 * @param refugee
	 *            the refugee to set
	 */
	public void setRefugee(Refugee refugee) {
		this.refugee = refugee;
	}

	/**
	 * @return the finded
	 */
	public boolean isFinded() {
		return finded;
	}

	/**
	 * @param finded
	 *            the finded to set
	 */
	public void setFinded(boolean finded) {
		this.finded = finded;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
}
