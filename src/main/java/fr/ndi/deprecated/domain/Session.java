package fr.ndi.deprecated.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import fr.ndi.deprecated.security.SessionIdentifierGenerator;

/**
 * A session
 * 
 * @author @Deprecated
 *
 */
@Entity
public class Session implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", nullable = false, updatable = false)
	@GeneratedValue
	private Long id;

	@ManyToOne
	private Seeker seeker;

	@ManyToOne
	private OngMember ongMember;

	@NotNull
	@Column(nullable = false)
	private String token;

	@Column(nullable = false)
	@NotNull
	@JsonIgnore
	private String ip;

	protected Session() {

	}

	/**
	 * Constructor
	 * 
	 * @param seeker
	 * @param ip
	 */
	public Session(Seeker seeker, String ip) {
		super();
		this.seeker = seeker;
		this.token = SessionIdentifierGenerator.nextSessionId();
		this.date = new Date();
		this.ip = ip;
	}

	public Session(OngMember ongMember, String ip) {
		super();
		this.ongMember = ongMember;
		this.token = SessionIdentifierGenerator.nextSessionId();
		this.date = new Date();
		this.ip = ip;
	}

	@NotNull
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the seeker
	 */
	public Seeker getSeeker() {
		return seeker;
	}

	/**
	 * @param seeker
	 *            the seeker to set
	 */
	public void setSeeker(Seeker seeker) {
		this.seeker = seeker;
	}

	/**
	 * @return the ongMember
	 */
	public OngMember getOngMember() {
		return ongMember;
	}

	/**
	 * @param ongMember
	 *            the ongMember to set
	 */
	public void setOngMember(OngMember ongMember) {
		this.ongMember = ongMember;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token
	 *            the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip
	 *            the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
