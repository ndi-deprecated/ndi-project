package fr.ndi.deprecated.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A ONG Member
 * 
 * @author @Deprecated
 *
 */
@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "mail" }), @UniqueConstraint(columnNames = { "phone" }) })
public class OngMember {

	@Id
	@Column(name = "id", nullable = false, updatable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne
	private Ong ong;

	@Column(nullable = false)
	@NotNull
	@Size(max = 128)
	private String lastName;

	@Column(nullable = false)
	@NotNull
	@Size(max = 128)
	private String firstName;

	@Column(nullable = true)
	@NotNull
	@Size(max = 15)
	private String phone;

	@Column(nullable = false)
	@NotNull
	@Size(max = 128)
	private String mail;

	@Column(nullable = false)
	@NotNull
	@Size(max = 128)
	@JsonIgnore
	private String password;

	@Column(nullable = false)
	@NotNull
	@JsonIgnore
	private int salt;

	protected OngMember() {

	}

	/**
	 * Constructor
	 * 
	 * @param ong
	 * @param lastName
	 * @param firstName
	 * @param phone
	 * @param mail
	 * @param password
	 */
	public OngMember(Ong ong, String lastName, String firstName, String phone, String mail, String password) {
		this.ong = ong;
		this.lastName = lastName;
		this.firstName = firstName;
		this.phone = phone;
		this.mail = mail;
		this.password = password;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the ong
	 */
	public Ong getOng() {
		return ong;
	}

	/**
	 * @param ong
	 *            the ong to set
	 */
	public void setOng(Ong ong) {
		this.ong = ong;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail
	 *            the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the salt
	 */
	public int getSalt() {
		return salt;
	}

	/**
	 * @param salt
	 *            the salt to set
	 */
	public void setSalt(int salt) {
		this.salt = salt;
	}

}
