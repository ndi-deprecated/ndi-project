package fr.ndi.deprecated.domain;

import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

/**
 * A camp of refugatee
 * 
 * @author @Deprecated
 *
 */
@Entity
public class Camp {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	private String organization;
	private String latitude;
	private String longitude;
	private String country;
	private int maxPeople;

	@ElementCollection
	@CollectionTable(name = "camp_has_refugee", joinColumns = @JoinColumn(name = "id"))
	private List<Long> refugeeIds;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the organization
	 */
	public String getOrganization() {
		return organization;
	}

	/**
	 * @param organization
	 *            the organization to set
	 */
	public void setOrganization(String organization) {
		this.organization = organization;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the refugeeIds
	 */
	public List<Long> getRefugeeIds() {
		return refugeeIds;
	}

	/**
	 * @param refugeeIds
	 *            the refugeeIds to set
	 */
	public void setRefugeeIds(List<Long> refugeeIds) {
		this.refugeeIds = refugeeIds;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the maxPeople
	 */
	public int getMaxPeople() {
		return maxPeople;
	}

	/**
	 * @param maxPeople
	 *            the maxPeople to set
	 */
	public void setMaxPeople(int maxPeople) {
		this.maxPeople = maxPeople;
	}

	/**
	 * 
	 * @return the actual people
	 */
	public int getActualPeople() {
		return refugeeIds.size();
	}
}
