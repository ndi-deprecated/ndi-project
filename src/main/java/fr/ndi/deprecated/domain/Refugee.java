package fr.ndi.deprecated.domain;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * A refugatee
 * 
 * @author @Deprecated
 *
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "lastName", "firstName" }))
// note: tant pis pour les homonymes
public class Refugee {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String lastName;
	private String firstName;
	private String country;
	private String phone;
	private Integer yearsOfBirth;
	private boolean publicProfile;

	// "le refugié il est con il a pas de mail"

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the publicProfile
	 */
	public boolean isPublicProfile() {
		return publicProfile;
	}

	/**
	 * @param publicProfile
	 *            the publicProfile to set
	 */
	public void setPublicProfile(boolean publicProfile) {
		this.publicProfile = publicProfile;
	}

	/**
	 * @return the yearsOfBirth
	 */
	public Integer getYearsOfBirth() {
		return yearsOfBirth;
	}

	/**
	 * @param yearsOfBirth
	 *            the yearsOfBirth to set
	 */
	public void setYearsOfBirth(Integer yearsOfBirth) {
		int years = Calendar.getInstance().get(Calendar.YEAR);
		if (yearsOfBirth != null && yearsOfBirth > years - 100 && yearsOfBirth < years)
			this.yearsOfBirth = yearsOfBirth;
	}
}
