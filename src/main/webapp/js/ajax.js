var root = "http://ndi2014.herokuapp.com";
var camp = "camps";
var refugee = "refugee";

var AddRefugeeController = function($scope, $http) {
	$scope.master = {};
	$scope.update = function(refugee) {
		$scope.master = angular.copy(refugee);
	};
	$scope.submit = function() {
		console.log($scope.refugee);
		$scope.master = angular.copy(refugee);
		$http.put(environment.urls.refugee, $scope.refugee).success(
				function(data) {
					// $scope.greeting = data;
				});
	};
	$scope.reset = function() {
		$scope.user = angular.copy($scope.master);

	};
	$scope.reset();
};

var AddCampController = function($scope, $http) {
	$scope.master = {};
	$scope.update = function(camp) {
		$scope.master = angular.copy(camp);
	};
	$scope.submit = function() {
		console.log($scope.camp);
		$scope.master = angular.copy(camp);
		$http.put(environment.urls.camps, $scope.camp).success(function(data) {
			// $scope.greeting = data;
		});
	};
	$scope.reset = function() {
		$scope.user = angular.copy($scope.master);

	};
	$scope.reset();
};

var AddONGController = function($scope, $http) {
	$scope.master = {};
	$scope.update = function(ong) {
		$scope.master = angular.copy(ong);
	};
	$scope.submit = function() {
		console.log($scope.ong);
		$scope.master = angular.copy(ong);
		$http.put(environment.urls.ong, $scope.ong).success(function(data) {
			// $scope.greeting = data;
		});
	};
	$scope.reset = function() {
		$scope.user = angular.copy($scope.master);

	};
	$scope.reset();
};

var SearchCampCtrl = function($scope, $http) {
	$scope.master = {};
	$scope.update = function(camp) {
		$scope.master = angular.copy(camp);
	};
	$scope.submit = function() {
		console.log($scope.camp);
		$scope.master = angular.copy(camp);
		$http.post(environment.urls.camps, $scope.camp).success(function(data) {
			// $scope.greeting = data;
		});
	};
	$scope.reset = function() {
		$scope.user = angular.copy($scope.master);
	};
	$scope.reset();
};

var SearchOngCtrl = function($scope, $http) {
	$scope.master = {};
	$scope.update = function(ong) {
		$scope.master = angular.copy(ong);
	};
	$scope.submit = function() {
		console.log($scope.ong);
		$scope.master = angular.copy(ong);
		$http.post(environment.urls.ong, $scope.ong).success(function(data) {
			// $scope.greeting = data;
		});
	};
	$scope.reset = function() {
		$scope.user = angular.copy($scope.master);
	};
	$scope.reset();
};
