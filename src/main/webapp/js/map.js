/**
 * Initialization of the Map
 */
function initMap() {
	/* Map */
	var map = L.map('map');

	/* Layer */
	L.tileLayer(
			'https://{s}.tiles.mapbox.com/v3/kevcoq.k2jjpd65/{z}/{x}/{y}.png',
			{
				maxZoom : 13,
				minZoom : 3
			}).addTo(map);

	/* environment Map */
	environment.map = {};
	environment.map.instance = map;
	environment.map.markers = new Array();

	/* Locate User */
	map.locate({
		setView : true,
		watch : true
	})
}

/**
 * Receive camps of refugees
 * 
 * @param name
 *            the name of the organization or null
 */
function receiveCamp(name) {
	$.ajax({
		type : "POST",
		url : environment.urls.camps,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		data : name == null ? '{"organization" : null}' : '{"organization": "'
				+ name + '"}',
		statusCode : {
			200 : function(result) {
				displayCamp(result);
			},
			400 : function(XHR, testStatus, errorThrown) {
				// TODO traiter message d'erreur
				console.log(XHR);
				console.log(testStatus);
				console.log(errorThrown);
			}
		}
	});
}

/**
 * Display the camp.
 * 
 * @param result
 */
function displayCamp(result) {
	for ( var center in result) {
		var marker = environment.map.markers[result[center].name];
		if (marker == undefined) {
			marker = new L.marker([ result[center].latitude,
					result[center].longitude ]).addTo(environment.map.instance);

			environment.map.markers[result[center].name] = marker;
		}
		var div = result[center].actualPeople / result[center].maxPeople;
		marker.bindPopup("<b>" + result[center].name + " ("
				+ result[center].organization
				+ ")</b><br/><span style=\"color:"
				+ (div > 0.75 ? "red" : div > 0.5 ? "orange" : "green") + "\">"
				+ result[center].actualPeople + " / "
				+ result[center].maxPeople + "</span>");
		marker.openPopup();
	}
	environment.map.instance.setZoom(5);
}
