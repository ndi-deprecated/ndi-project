"use strict";

app.factory("Camps", function ($http) {
    var API_URI = 'http://ndi2014.herokuapp.com/camps';
    return {
        fetch : function() {
            return $http.post(API_URI, {"organization":null});
        }
    };
});

