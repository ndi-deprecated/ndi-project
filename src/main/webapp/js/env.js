/**
 * Create environment
 */
function createEnv() {
	environment = {};

	/** ####### URL ###### */
	//environment.host = "http://private-8188d-deprecatedndi.apiary-mock.com";
	environment.host = "http://ndi2014.herokuapp.com";

	environment.urls = [];
	environment.urls.refugee = environment.host + "/refugee/";
	environment.urls.camps = environment.host + "/camps";
	environment.urls.camp = environment.host + "/camp/";
	environment.urls.ong = environment.host + "/ong/";
}
